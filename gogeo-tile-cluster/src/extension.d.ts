
declare namespace L {
  export interface Cluster {
    count: number;
  }

  export interface TileClusterOptions extends L.LayerOptions {
    subdomains?: string | string[];
    minZoom?: number;
    maxZoom?: number;
    tileSize?: number;
    useJsonp?: boolean;
    pointerCursor?: boolean;
    createIcon?: (cluster: Cluster) => L.DivIcon;
    calculateClusterQtd?: (zoom: number) => number;
  }

  export function tileCluster(url: string, options: TileClusterOptions);
}
