import { L } from "@gogeo/leaflet";
import { StringBuilder } from "@gogeo/formatter";
import * as fetch from "isomorphic-fetch";


async function loadFrom(url: string, zoom: number) {
  const response = await fetch(url, { method: "GET" })
  const data = await response.json();

  return {
    data,
    zoom
  };
}

export interface Cluster {
  count: number;
}

export interface TileClusterOptions extends L.LayerOptions {
  subdomains?: string | string[];
  minZoom?: number;
  maxZoom?: number;
  tileSize?: number;
  useJsonp?: boolean;
  pointerCursor?: boolean;
  createIcon?: (cluster: Cluster) => L.DivIcon;
  calculateClusterQtd?: (zoom: number) => number;
}

export class TileCluster extends L.Layer {
  private static _nextJsonpId = 0;
  private static _jsonp_prefix: string = "cl_us_ter_";

  options: TileClusterOptions = {
    subdomains: "abc",
    minZoom: 1,
    maxZoom: 18,
    tileSize: 256,
    useJsonp: false,
    pointerCursor: true
  };

  private _url: string;
  private _cache = {};
  private _group: L.FeatureGroup;
  private _jsonpId: string;
  private _map: L.Map;
  private _container: HTMLElement;
  private _convexHull: L.Polygon;

  constructor(url?: string, options?: TileClusterOptions) {
    super();
    
    Object.assign(this.options, options);
    this._url = url;
    this._cache = {};
    this._group = L.featureGroup();
    this.verifyJsonpOptions();

    this.options.createIcon = this.options.createIcon || TileCluster.defaultCreateIcon;
    this.options.calculateClusterQtd = this.options.calculateClusterQtd || TileCluster.calculateClusterQtd;

    if (this.options.useJsonp) {
      this._jsonpId = this.generateJsonpId();
    }
  }

  private verifyJsonpOptions() {
    const url = this._url;

    if (url.match(/callback={cb}/) && !this.options.useJsonp) {
      throw new Error("Must set useJsonp options if you want to use a callback");
    }

    if (!url.match(/callback={cb}/) && this.options.useJsonp) {
      throw new Error("Must add callback={cb} url parameter to use with JSONP mode");
    }
  }

  private generateJsonpId() {
    return `${TileCluster._jsonp_prefix}${TileCluster._nextJsonpId++}`;
  }

  onAdd(map: L.Map): this {
    this._map = map;
    this._container = this._map.getContainer();
    
    this._group.addTo(this._map);
    this.update();

    const zoom = this._map.getZoom();

    if (zoom > this.options.maxZoom || zoom < this.options.minZoom) {
      return this;
    }

    this._group.on("mouseover", this.drawConvexHull, this);
    this._group.on("mouseout", this.removeConvexHull, this);

    map.on("moveend", this.update, this);
    map.on("zoomend", this.update, this);

    return this;
  }

  private update() {
    this.clearClusters();
  }

  private clearClusters() {
    this.removeConvexHull();
    this._group.clearLayers();
  }

  private removeConvexHull() {
    const context = this._convexHull;

    if (context && this._map.hasLayer(context)) {
      this._map.removeLayer(context);
    }

    this._convexHull = null;
  }

  private drawConvexHull(event: L.MouseEvent) {
    if (this._convexHull) {
      return;
    }

    throw new Error("Not implemented");
  }

  private static defaultCreateIcon(cluster?: Cluster) {
    console.warn("Verificar se existe a propriedade count");

    const childCount: number = cluster["count"] || 0;
    const markerClassBuilder = new StringBuilder();

    markerClassBuilder.append("marker-cluster marker-cluster-");

    if (childCount < 100) {
      markerClassBuilder.append("small");
    } else if (childCount < 10000) {
      markerClassBuilder.append("medium");
    } else if (childCount < 100000) {
      markerClassBuilder.append("large");
    } else {
      markerClassBuilder.append("extra-large");
    }

    const markerClass = markerClassBuilder.toString();

    let iconPoint: L.Point;
    let rootClass: string;

    if (childCount >= 100000) {
      iconPoint = L.point(55, 55);
      rootClass = "large";

    } else if (childCount >= 10000) {
      iconPoint = L.point(45, 45);
      rootClass = "medium";

    } else {
      iconPoint = L.point(40, 40)
      rootClass = "small";
    }

    return L.divIcon({
      html: `<div class="${rootClass}"><span>${childCount}</span></div>`,
      className: markerClass,
      iconSize: iconPoint
    });
  }

  private static calculateClusterQtd(zoom: number) {
    return 1;
  }
}

export function tileCluster(url: string, options: TileClusterOptions) {
  return new TileCluster(url, options);
}

L.tileCluster = tileCluster;
