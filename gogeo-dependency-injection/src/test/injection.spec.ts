import { Container, singleton, transient, autoInject } from "../index";

@transient
class Item {

}

@singleton
class SingletonItem extends Item {

}

@autoInject
@transient
class List {
  constructor(public item: Item) {

  }
}

describe("O Container de injeção de dependências", () => {
  const container = new Container();
  container.makeGlobal();

  it("deve aceitar a injeção em classes", () => {
    const item = container.resolve(Item);
    const list = container.resolve(List);

    expect(list.item).not.toBe(item);
    expect(list.item).not.toBe(container.resolve(SingletonItem));
  });

  it("deve suportar a definição de singletons", () => {
    const singleItem = container.resolve(SingletonItem);
    const singleItem2 = container.resolve(SingletonItem);

    expect(singleItem).toBe(singleItem2);
  });
});
