import "reflect-metadata";
import * as audi from "aurelia-dependency-injection";

export interface Type<T> {
  new (...args: any[]): T;
}

export class Container extends audi.Container {
  constructor() {
    super();
  }

  resolve<T>(type: Type<T>): T {
    return this.get(type);
  }

  registerType<T>(type: Type<T>) {
    super.autoRegister(type);
  }
}

export const singleton: ClassDecorator = audi.singleton();
export const transient: ClassDecorator = audi.transient();
export const autoInject: ClassDecorator = audi.autoinject;
