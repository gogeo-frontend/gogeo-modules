declare module "@gogeo/react" {
  import * as React from "react";
  import * as ReactDOM from "react-dom";
  
  export {
    React,
    ReactDOM
  };
}
