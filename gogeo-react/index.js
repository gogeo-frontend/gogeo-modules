var React = require("react");
var ReactDOM = require("react-dom");

module.exports = {
  React: React,
  ReactDOM: ReactDOM
};
