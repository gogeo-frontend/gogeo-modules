import { React } from "@gogeo/react";
import { Select, SelectAsync } from "@gogeo/react-select";

export class SomeComponent extends React.Component<{}, {}> {
  render() {
    return (
      <div>
        <Select labelKey="text" />
        <SelectAsync loadOptions={null} />
      </div>
    );
  }
}
