var Select = require("react-select");
var Async = Select.Async;

module.exports = { Select, SelectAsync };
