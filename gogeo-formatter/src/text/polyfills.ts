export function startsWith(self: string, text: string): boolean {
  return new RegExp("^" + text).test(self);
}

export function endsWith(self: string, text: string): boolean {
  return new RegExp(text + "$").test(self);
}

if (typeof String.prototype['startsWith'] !== "function") {
  String.prototype["startsWith"] = function (other: string): boolean {
    return startsWith(this, other);
  };
}

if (typeof String.prototype["endsWith"] !== "function") {
  String.prototype["endsWith"] = function (other: string): boolean {
    return endsWith(this, other);
  };
}
