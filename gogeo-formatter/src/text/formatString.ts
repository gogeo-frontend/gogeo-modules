import * as moment from "moment";
import * as numeral from "numeral";

const formatProvider = {
  date: (fromFormatOrToFormat: string, toFormat?: string) => (value: Date | string) => {
    if (!toFormat) {
      toFormat = fromFormatOrToFormat;
      fromFormatOrToFormat = undefined;
    }

    return value instanceof Date
      ? moment(value).format(toFormat)
      : moment(value, fromFormatOrToFormat).format(toFormat);
  },

  number: (format: string) => (value: number) => {
    return numeral(value).format(format);
  }
};

export function formatString(format: string, ...args: any[]): string {
  const date = formatProvider.date;
  const number = formatProvider.number;

  return format.replace(/\{(\d+)(:.*)?\}/g, (_, ...groups) => {
    const [indexString, formatString] = groups;
    const index = parseInt(indexString);
    const value = args[index];

    if (formatString) {
      const scope = {
        date,
        number,
        D: date,
        N: number,
        resolveFormat() {
          let invoker: string = formatString.substring(1);

          if (invoker.indexOf("(") == -1) {
            invoker += "()";
          }

          return eval("this." + invoker);
        }
      };

      const formatFunction = scope.resolveFormat();

      if (formatFunction) {
        return formatFunction(value);
      }
    }

    return value;
  });
}
