import { formatString } from "./formatString";

export class StringBuilder {
  private _parts: string[] = [];

  constructor() {

  }

  append(value: string) {
    if (value) {
      this._parts.push(value);
    }

    return this;
  }

  appendFormat(format: string, ...args: any[]) {
    this._parts.push(formatString.apply(null, [format, ...args]));

    return this;
  }

  toString() {
    return this._parts.join("");
  }
}
