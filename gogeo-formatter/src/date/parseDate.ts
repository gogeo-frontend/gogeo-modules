import * as moment from "moment";

export function parseDate(value: string, format?: string) {
  return moment(value, format).toDate();
}
