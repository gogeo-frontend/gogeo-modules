import * as moment from "moment";

export function formatDate(format: string, value: string | Date): string {
  return moment(value).format(format);
}
