import * as numeral from "numeral";

export function parseNumber(formatted: string): number {
  return numeral().unformat(formatted);
}
