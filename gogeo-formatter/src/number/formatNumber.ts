import * as numeral from "numeral";

export function formatNumber(format: string, value: string | number): string {
  return numeral(value).format(format);
}
