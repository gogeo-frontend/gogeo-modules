import * as moment from "moment";
import * as numeral from "numeral";

import {
  formatString as _formatString,
  StringBuilder
} from "./text/index";

import {
  formatNumber as _formatNumber,
  parseNumber as _parseNumber
} from "./number/index";

import {
  formatDate as _formatDate,
  parseDate as _parseDate
} from "./date/index";


export module Formatter {
  export const formatString = _formatString;

  export const formatDate = _formatDate;
  export const parseDate = _parseDate;

  export const formatNumber = _formatNumber;
  export const parseNumber = _parseNumber;

  export const withMoment = moment;
  export const withNumeral = numeral;

  export function beginBuild(text?: string) {
    return new StringBuilder().append(text);
  }
}
