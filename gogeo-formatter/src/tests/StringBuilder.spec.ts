import { Formatter } from "../index";

describe("O StringBuilder", () => {
  it("deve retornar vazio quando não configurado", () => {
    const builder = Formatter.beginBuild();

    expect(builder.toString()).toBe("");
  });

  it("deve retornar o próprio texto quando houver somente uma adição", () => {
    const message = "oi";
    const builder = Formatter.beginBuild();

    builder.append(message);

    expect(builder.toString()).toBe(message);
  });

  it("deve concatenar as strings quando houverem adições", () => {
    const message = "oi";
    const builder = Formatter.beginBuild();

    builder.append(message);
    builder.append(message);

    expect(builder.toString()).toBe(message + message);
  });

  it("deve permitir adicionar um texto formatado, no mesmo formato do formatString", () => {
    const builder = Formatter.beginBuild();

    builder.appendFormat("Hello, {0}", "Daniel");

    expect(builder.toString()).toBe("Hello, Daniel");
  });

  it("deve permitir adicionar uma data formatada, no mesmo formato do formatString", () => {
    const builder = Formatter.beginBuild();
    const date = new Date();

    date.setFullYear(2016);
    date.setMonth(0);

    builder.appendFormat("A data é {0:date('MM/YYYY')}", date);

    expect(builder.toString()).toBe("A data é 01/2016");
  });

  it("deve permitir adicionar um número formatado, no mesmo formato do formatString", () => {
    const builder = Formatter.beginBuild();

    builder.appendFormat("Valor = {0:N}", 10.0);

    expect(builder.toString()).toBe("Valor = 10");
  });
});
