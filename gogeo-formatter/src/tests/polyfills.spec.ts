import { startsWith, endsWith } from "../index";

describe("O polyfill de string", () => {
  it("deve prover implementação para startsWith", () => {
    expect("Daniel".startsWith("Dan")).toBe(true);
    expect("Daniel".startsWith("dan")).toBe(false);

    expect(startsWith("Daniel", "Dan")).toBe(true);
    expect(startsWith("Daniel", "dan")).toBe(false);
  });

  it("deve prover implementação para endsWith", () => {
    expect("Daniel Alves".endsWith("Alves")).toBe(true);
    expect("Daniel Alves".endsWith("e")).toBe(false);

    expect(endsWith("Daniel Alves", "Alves")).toBe(true);
    expect(endsWith("Daniel Alves", "e")).toBe(false);
  });
});
