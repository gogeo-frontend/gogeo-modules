import { Formatter } from "../index";

describe("formatString", () => {
  const formatString = Formatter.formatString;

  it("deve ser capaz de formatar uma string sem parâmetros", () => {
    const message = "Hello";
    const result = formatString(message);

    expect(result).toBe(message);
  });

  it("deve ser capaz de substituir um parâmetro", () => {
    const result = formatString("Hello, {0}", "goGEO");

    expect(result).toBe("Hello, goGEO");
  });

  it("deve ser capaz de substituir vários um parâmetros", () => {
    const result = formatString("Hello, {0} {1}", "Super", "goGEO");

    expect(result).toBe("Hello, Super goGEO");
  });

  it("deve ser capaz de substituir vários um parâmetros repetidos", () => {
    const result = formatString("Hello, {0} {0}", "Super");

    expect(result).toBe("Hello, Super Super");
  });

  it("deve ser capaz de substituir e formatar datas", () => {
    const date = new Date();
    date.setFullYear(2016);
    date.setMonth(0);

    const result = formatString("{0:D('YYYY/MM')}", date);
  

    expect(result).toBe("2016/01");
  });

  it("deve ser capaz de substituir e formatar números", () => {
    const result = formatString("{0:N('0.00')}", 10.0);
  
    expect(result).toBe("10.00");
  });
});
