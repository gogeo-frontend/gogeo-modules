
declare module "@gogeo/react-maskedinput" {
  import { React } from "@gogeo/react";

  interface MaskedInputProps {
    mask: string;
    placeholder?: string;
    size?: number | string;
    value?: string;
    onChange?: (e: React.SyntheticEvent<HTMLInputElement>) => any;
    onBlur?: (e: React.SyntheticEvent<HTMLInputElement>) => any;
  }

  export class MaskedInput extends React.Component<MaskedInputProps, {}>{
    mask: {
      getValue(): string;
      setValue(value: string): void;
    };
  }
}
