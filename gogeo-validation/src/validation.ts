import { formatString } from "@gogeo/formatter";


export interface ValidationError {
  propertyName?: string;
  errorMessage: string;
}

export class Validator {
  private _errors: ValidationError[] = [];
  private _displayErrors: { [key: string]: string; } = {};

  get errors(): ValidationError[] {
    return this._errors;
  }

  get displayErrors() {
    return this._displayErrors;
  }

  set displayErrors(value) {
    this._displayErrors = value;
  }

  validate<TRoot>(value: TRoot) {
    return new Specification(this, value);
  }

  add(propertyName: string, errorMessage: string) {
    this.addError({
      propertyName,
      errorMessage
    });
  }

  addError(error: ValidationError) {
    this._errors.push(error);
    return this;
  }

  getDisplayName(propertyName: string, path: string): string {
    return this._displayErrors[path]
      || this._displayErrors[propertyName]
      || propertyName;
  }

}

function extractMemberPath<TRoot, TProperty>(selector: (root: TRoot) => TProperty) {
  const pattern = /function\s*.*\((\w*)\)\s*\{\s*return\s*\1\.([a-zA-Z_$][a-zA-Z0-9_$]*);?\s*}/;
  const match = pattern.exec(selector.toString());
  const memberPath = match[2];

  return memberPath;
}

function combinePath(rootPath: string, path: string): string {
  return rootPath ? `${rootPath}.${path}` : path;
}


export interface RuleContext<TRoot, TProperty> {
  root: TRoot,
  propertyValue: TProperty,
  propertyName: string;
  validator: Validator;
  rule: Specification<TRoot>;
}


export type Rule<TRoot, TProperty> = (context: RuleContext<TRoot, TProperty>) => string | null;

export class Specification<TRoot> {
  constructor(private _validator: Validator,
    private _root: TRoot,
    private _path: string = "") {

  }

  get path(): string {
    return this._path;
  }

  check<TProperty>(selector: (root: TRoot) => TProperty) {
    const propertyName = extractMemberPath(selector);
    const root = this._root;
    const propertyPath = combinePath(this._path, propertyName);

    const self = this;
    const validator = this._validator;

    return {
      by(rulesSelector: (checker: AssertRules<TRoot>) => Rule<TRoot, TProperty>[]) {
        const rules = new AssertRules<TRoot>();
        const chosenRules = rulesSelector(rules);

        const context: RuleContext<TRoot, TProperty> = {
          root,
          propertyValue: selector(root),
          propertyName,
          validator,
          rule: self
        };

        chosenRules.forEach(rule => {
          try {
            const error = rule(context);

            if (error) {
              self._validator.addError({
                propertyName: propertyPath,
                errorMessage: error
              });
            }

          } catch (e) {
            self._validator.addError({
              propertyName: propertyPath,
              errorMessage: e.toString()
            });
          }
        });

        return self;
      }
    };
  }

  descend<TProperty>(selector: (root: TRoot) => TProperty, ruleConfigurator: (rule: Specification<TProperty>, value: TProperty) => void) {
    const memberPath = extractMemberPath(selector);
    const path = combinePath(this._path, memberPath);

    const memberValue = selector(this._root);
    const rule = new Specification(this._validator, memberValue, path);

    ruleConfigurator(rule, memberValue);

    return this;
  }

  descendEach<TItem>(selector: (root: TRoot) => TItem[], ruleConfigurator: (rule: Specification<TItem>, value: TItem) => void) {
    const memberPath = extractMemberPath(selector);
    const membersPath = combinePath(this._path, memberPath);
    const memberArrayValue = selector(this._root);

    for (let i = 0; i < memberArrayValue.length; i++) {
      const memberValue = memberArrayValue[i];
      const memberPath = `${membersPath}[${i}]`;
      const rule = new Specification(this._validator, memberValue, memberPath);

      ruleConfigurator(rule, memberValue);
    }

    return this;
  }

}

export class AssertRules<TRoot> {

  notNull(errorMessage: string = "Campo '{0}' é requerido."): Rule<TRoot, any> {
    return ({ propertyValue, propertyName, validator, rule }) =>
      !propertyValue
        ? formatString(errorMessage, validator.getDisplayName(propertyName, rule.path))
        : null;
  }

  notEmpty(errorMessage: string = "Campo '{0}' deve ser preenchido."): Rule<TRoot, string> {
    return ({ propertyValue, propertyName, validator, rule }) =>
      !propertyValue
        ? formatString(errorMessage, validator.getDisplayName(propertyName, rule.path))
        : null;
  }

  notEmptyArray<TProperty>(errorMessage: string = "Campo '{0}' requer ao menos um item."): Rule<TRoot, TProperty[]> {
    return ({ propertyValue, propertyName, validator, rule }) =>
      !propertyValue || !propertyValue.length
        ? formatString(errorMessage, validator.getDisplayName(propertyName, rule.path))
        : null;
  }

  notEqual<TProperty>(value: TProperty, errorMessage: string = "Campo '{0}' deve ser diferente de {1}."): Rule<TRoot, TProperty> {
    return ({ propertyValue, propertyName, validator, rule }) =>
      propertyValue === value
        ? formatString(errorMessage, validator.getDisplayName(propertyName, rule.path))
        : null;
  }

  greaterThan(value: number, errorMessage: string = "Campo '{0}' deve ser maior que {1}."): Rule<TRoot, number> {
    return ({ propertyValue, propertyName, validator, rule }) =>
      propertyValue < value
        ? formatString(errorMessage, validator.getDisplayName(propertyName, rule.path), value)
        : null;
  }

  custom<TProperty>(handler: Rule<TRoot, TProperty>) {
    return handler;
  }

}
