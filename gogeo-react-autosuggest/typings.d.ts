declare module "react-autosuggest" {
  import { React } from "@gogeo/react";

  export interface SuggestionUpdateRequest {
    value: string;
    reason: string;
  }

  export interface InputValues {
    value: string;
    valueBeforeUpDown?: string;
  }

  export interface InputProps extends React.HTMLAttributes<HTMLElement> {
    value: string;
    onChange: (event: React.FormEvent<HTMLElement>, params?: { newValue: string, method: string }) => void;
  }

  export interface SuggestionSelectedEventData {
    method: string;
    sectionIndex: number;
    suggestion: any;
    suggestionValue: string;
  }

  export interface Theme {
    container: string;
    containerOpen: string;
    input: string;
    sectionContainer: string;
    sectionSuggestionsContainer: string;
    sectionTitle: string;
    suggestion: string;
    suggestionFocused: string;
    suggestionsContainer: string;
  }

  export interface AutosuggestProps extends React.Props<Autosuggest> {
    suggestions: any[];
    onSuggestionsUpdateRequested?: (request: SuggestionUpdateRequest) => void;
    getSuggestionValue: (suggestion: any) => string;
    renderSuggestion: (suggestion: any, inputValues: InputValues) => JSX.Element;
    inputProps: InputProps;
    shouldRenderSuggestions?: (value: string) => boolean;
    multiSection?: boolean;
    renderSectionTitle?: (section: any, inputValues: InputValues) => JSX.Element;
    getSectionSuggestions?: (section: any) => any[];
    onSuggestionSelected?: (event: React.FormEvent<HTMLElement>, data: SuggestionSelectedEventData) => void;
    focusInputOnSuggestionClick?: boolean;
    theme?: Theme;
    id?: string;
  }

  export class Autosuggest extends React.Component<AutosuggestProps, any> {

  }
}
