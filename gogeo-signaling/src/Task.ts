
export class Task<TState, TInput extends {}> {
  constructor(public signalName: string,
    public state: TState,
    public input: TInput) {

  }
}
