import * as Rx from "rxjs/Rx";
import { action } from "mobx";
import { Result, success, failure } from "@gogeo/http-client";
import { Task } from "./Task";
import { executor, AbortBehaviorError } from "./Executor";


export interface BehaviorWithinSetup<TState, TInput, TNewState, TOutput> {
  mapState: (state: TState, input: TInput) => TNewState;
  behavior: (behavior: Behavior<TNewState, TInput>) => Behavior<any, TOutput>;
}


export class Behavior<TState, TInput extends {}> {
  constructor(
    public signalName: string,
    public task$$: Rx.Observable<Rx.Observable<Task<TState, TInput>>>) {

  }

  mapState<TNewState>(transformer: (state: TState, input: TInput) => TNewState): Behavior<TNewState, TInput> {
    const transformerName = transformer.name || "<mapState>";

    return new Behavior<TNewState, TInput>(
      this.signalName,
      this.task$$.map(task$ => task$.map(
        action(transformerName, (task: Task<TState, TInput>) => {
          const state = task.state;
          const input = task.input;
          const newState = transformer(state, input);

          return new Task(task.signalName, newState, task.input);
        })
      ))
    );
  }

  then<TOutput extends {}>(handler: (state: TState, input: TInput) => TOutput): Behavior<TState, TInput & TOutput> {
    const mapperName = handler.name || "<then>";

    return new Behavior<TState, TInput & TOutput>(
      this.signalName,
      this.task$$.map(task$ => task$.map(
        action(mapperName, (task: Task<TState, TInput>) => {
          const input: TInput = task.input || <TInput>{};
          const output = handler(task.state, input);
          const nextInput = Object.assign(input, output);

          return new Task(task.signalName, task.state, nextInput);
        })
      ))
    );
  }

  thenAwait<TResult extends {}>(handler: (state: TState, input: TInput) => Promise<TResult>) {
    return {
      continueWith: <TOutput>(continueHandler: (state: TState, input: TInput, result: Result<TResult>) => TOutput): Behavior<TState, TInput & TOutput> => {
        const continueHandlerName = continueHandler.name || "<continueWith>";

        return new Behavior<TState, TInput & TOutput>(
          this.signalName,
          this.task$$.map(task$ => task$
            .flatMap(task => {
              const input: TInput = task.input || <TInput>{};
              let promise: Promise<TResult>;

              try {
                promise = handler(task.state, input);
              } catch (e) {
                promise = Promise.reject(e);
              }

              const resultPromise =
                promise
                  .then(result => success(result))
                  .catch(error => failure(error))
                  .then(result => ({ result, task }));

              return Rx.Observable.from(resultPromise);
            })
            .map(action(continueHandlerName, (resultAndTask: { result: Result<TResult>; task: Task<TState, TInput>; }) => {
              const { result, task } = resultAndTask;
              const { state, input } = task;
              const output = continueHandler(state, input, result);
              const nextInput = Object.assign(input, output);

              return new Task(task.signalName, task.state, nextInput);
            }))
          ));
      }
    };
  }

  when(predicate: (state: TState, input: TInput) => boolean, handler: (state: TState, input: TInput) => void): Behavior<TState, TInput> {
    handler = action(handler.name || "<when>", handler);

    return new Behavior<TState, TInput>(
      this.signalName,
      this.task$$.map(task$ =>
        task$.do((task: Task<TState, TInput>) => {
          if (predicate(task.state, task.input)) {
            handler(task.state, task.input);
          }
        })
      )
    );
  }

  thenEmit(handler: (state: TState, input: TInput) => void) {
    handler = action(handler.name || "<thenEmit>", handler);

    return new Behavior<TState, TInput>(
      this.signalName,
      this.task$$.map(task$ =>
        task$.do((task: Task<TState, TInput>) => {
          handler(task.state, task.input);
        }))
    );
  }

  thenWithin<TNewState, TOutput>(config: BehaviorWithinSetup<TState, TInput, TNewState, TOutput>): Behavior<TState, TInput & TOutput> {
    const task$$ = this.task$$.map(
      task$ => task$.flatMap(
        task => {
          // const state = task.state;
          const input = task.input;
          const newInput = Object.assign({}, input);

          const innerTask = new Task(
            this.signalName,
            config.mapState(task.state, newInput),
            newInput
          );

          const innerTask$ = Rx.Observable.of(innerTask);
          const innerBehavior = new Behavior(this.signalName, Rx.Observable.of(innerTask$));
          const endBehavior = config.behavior(innerBehavior);

          return new Rx.Observable<Task<TState, TInput & TOutput>>((observer: Rx.Observer<Task<TState, TOutput>>) => {
            return endBehavior.task$$.flatMap(task$ => task$).subscribe({
              next: (innerTask) => {
                observer.next(new Task(task.signalName, task.state, Object.assign({}, task.input, innerTask.input)))
              },
              error: (error) => observer.error(error),
              complete: () => observer.complete()
            });
          });
        }
      )
    );

    return new Behavior<TState, TInput & TOutput>(this.signalName, task$$);
  }

  debounce(time: number) {
    return new Behavior(
      this.signalName,
      this.task$$.debounceTime(time)
    );
  }

  abortWhen(predicate: (state: TState, input: TInput) => boolean) {
    return new Behavior(
      this.signalName,
      this.task$$.map(task$ => task$.map(task => {
        if (predicate(task.state, task.input)) {
          throw new AbortBehaviorError();
        }

        return task;
      }))
    );
  }

  activate(): this {
    executor.registerBehavior(this);
    return this;
  }
}
