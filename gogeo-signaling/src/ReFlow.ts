import * as Rx from "rxjs/Rx";
import { action } from "mobx";
import { Nothing } from "@gogeo/http-client";
import { Task } from "./Task";
import { Signal, createSignal } from "./Signal";
import { executor } from "./Executor";
import { Behavior } from "./Behavior";


export function signal(name: string) {
  return {
    withState<TState>() {
      return createSignal<TState, Nothing>(name);
    },

    withStateAndInput<TState, TInput>() {
      return createSignal<TState, TInput>(name);
    }
  };
}


export function afterSignal<TState, TInput>(signal: Signal<TState, TInput>, handler: (state: TState, input: TInput) => void): Rx.Subscription {
  return executor.onEmitted(signal).subscribe(action(
    (task: Task<TState, TInput>) => {
      handler(task.state, task.input);
    })
  );
}

export function afterSignals<TState, TInput>(
  signals: Signal<TState, TInput>[],
  handler: (state: TState, input: TInput) => void,
  options?: { debounce?: number; throttle?: number; }): Rx.Subscription {

  let tasks$ =
    Rx.Observable.from(signals)
      .flatMap(signal => executor.onEmitted(signal).map(task => ({ signal, task })));

  if (options) {
    if (options.debounce) {
      tasks$ = tasks$.debounceTime(options.debounce);
    } else if (options.throttle) {
      tasks$ = tasks$.throttleTime(options.throttle);
    }
  }

  return tasks$.subscribe(x => {
    // const signal = x.signal;
    const state = x.task.state;
    const input = x.task.input;

    action(
      () => {
        try {
          handler(state, input);
        } catch (e) {
          console.error(e);
        }
      }
    )();
  });
}


export function behaviorFor<TState, TInput extends {}>(signal: Signal<TState, TInput>) {
  return new Behavior<TState, TInput>(
    signal.signalName,
    signal.source$
      .map((task$: Rx.Observable<Task<any, any>>) => task$.do((task: Task<any, any>) => {
        console.log(
          `%c[SIGNAL]%c '${task.signalName}'`,
          "background: purple; color: white; font-weight: bold; line-height: 30px;",
          "color: purple; line-height: 30px;");
      }))
  );
}

export * from "./Task";
export * from "./Signal";
export * from "./Executor";
export * from "./Behavior";
