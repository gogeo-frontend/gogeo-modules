import * as Rx from "rxjs/Rx";
import { Task } from "./Task";

export interface Signal<TState, TInput extends {}> {
  signalName: string;
  source$: Rx.Subject<Rx.Observable<Task<TState, TInput>>>;
  (state: TState, input?: TInput): void;
}

export function createSignal<TState, TInput extends {}>(name: string): Signal<TState, TInput> {
  const source$ = new Rx.Subject<Rx.Observable<Task<TState, TInput>>>();

  const signal: Signal<TState, TInput> = Object.assign(
    function (state: TState, input: TInput = <TInput>{}) {
      const task = new Task<TState, TInput>(name, state, input);
      
      source$.next(Rx.Observable.of(task));
    },

    {
      signalName: name,
      source$
    }
  );

  return signal;
}
