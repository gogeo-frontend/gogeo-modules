import * as Rx from "rxjs/Rx";
import { Task } from "./Task";
import { Signal } from "./Signal";
import { Behavior } from "./Behavior";


interface Logger {
  error(message?: any, ...optionalParams: any[]): void;
  group(groupTitle?: string): void;
  groupCollapsed(groupTitle?: string): void;
  groupEnd(): void;
  log(message?: any, ...optionalParams: any[]): void;
  warn(message?: any, ...optionalParams: any[]): void;
}

function noOp() {

}

export class AbortBehaviorError extends Error {
  constructor(message?: string) {
    super(message);
  }
}

export class Executor {
  private readonly _tasks$$ = new Rx.Subject<Rx.Observable<Task<any, any>>>();
  private readonly _emitted$ = new Rx.Subject<Task<any, any>>();
  private readonly _behaviorSubscriptions: { [key: string]: Rx.Subscription; } = {};
  private readonly _logger: Logger;

  constructor() {
    this._logger = (console != null)
      ? console
      : {
        log: noOp,
        warn: noOp,
        error: noOp,
        group: noOp,
        groupEnd: noOp,
        groupCollapsed: noOp
      };

    this._tasks$$
      .concatMap((task$: Rx.Observable<Task<any, any>>) => new Rx.Observable<Task<any, any>>((observer: Rx.Observer<Task<any, any>>) => {
        return task$.subscribe({
          next: task => {
            this._logger.log(
              "%c[SIGNAL END]%c %s",
              "background: orangered; color: white; font-weight: bold",
              "color: orangered; line-height: 30px;",
              task.signalName);

            observer.next(task);
          },
          error: error => {
            if (error instanceof AbortBehaviorError) {
              this._logger.log(
                "%c[SIGNAL ABORT]%c %s",
                "background: orangered; color: white; font-weight: bold",
                "color: orangered; line-height: 30px;",
                error);

            } else {
              this._logger.log(
                "%c[SIGNAL ERROR]",
                "background: red; color: white; font-weight: bold; font-height: 18px; line-height: 30px;");
            }

            observer.next(error);
            observer.complete();
          },
          complete: () => {
            observer.next(null);
            observer.complete();
          }
        });
      }))
      .subscribe({
        next: (taskOrError: Task<any, any> | Error) => {
          // ignored
          if (taskOrError instanceof Task) {
            this._emitted$.next(taskOrError);

          } else if (taskOrError) {
            this._logger.error(taskOrError);
          }
        },

        error: (error) => {
          this._logger.error("THIS SHOULD NEVER BE EXECUTED!! BUWAHAHAA!", error);
        }
      });
  }

  onEmitted<TState, TInput extends {}>(signal: Signal<TState, TInput>): Rx.Observable<Task<TState, TInput>> {
    return this._emitted$.filter(x => x.signalName === signal.signalName);
  }

  registerBehavior<TState, TInput extends {}>(behavior: Behavior<TState, TInput>): Rx.Subscription {
    const subscription = behavior.task$$.subscribe(task$ => this._tasks$$.next(task$));
    const oldSubscription = this._behaviorSubscriptions[behavior.signalName];

    if (oldSubscription) {
      oldSubscription.unsubscribe();
    }

    this._behaviorSubscriptions[behavior.signalName] = subscription;

    return subscription;
  }
}

export const executor = new Executor();
