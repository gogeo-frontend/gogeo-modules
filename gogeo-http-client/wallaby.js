
module.exports = function() {
  "use strict";
  return {
    files: [
      "src/**/*.ts",
      "!src/tests/**"
    ],
    tests: [
      "src/tests/**/*.spec.ts"
    ],
    env: {
      type: "node",
      runner: "node",
      params: {
        runner: "--harmony"
      }
    },
    testFramework: "jest"
  };
};
