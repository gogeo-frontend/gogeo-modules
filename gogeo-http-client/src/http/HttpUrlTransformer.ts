
/**
 * Transforma uma url e queries (parâmetros) em uma única url.
 */
export interface HttpUrlTransformer {
  
  /**
   * Mapeia a url especificada e seus parâmetros para uma nova url. 
   */
  map(url: string, query?: any): string;

}
