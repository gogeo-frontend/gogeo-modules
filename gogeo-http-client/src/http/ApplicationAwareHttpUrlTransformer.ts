import { SimpleHttpUrlTransformer } from "./SimpleHttpUrlTransformer";

export class ApplicationAwareHttpUrlTransformer extends SimpleHttpUrlTransformer {

  constructor(private readonly _baseUrl: string) {
    super();

    if (!this._baseUrl.endsWith("/")) {
      this._baseUrl += "/";
    }
  }

  map(url: string, query?: any): string {
    if (url.startsWith("~/")) {
      url = this._baseUrl + url.substring(2);
    }

    return super.map(url, query);
  }

}
