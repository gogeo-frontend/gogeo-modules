import "isomorphic-fetch";
import { HttpUrlTransformer } from "./HttpUrlTransformer";
import { SimpleHttpUrlTransformer } from "./SimpleHttpUrlTransformer";
import { HttpMessagePackager } from "./packaging/HttpMessagePackager";
import { JsonHttpMessagePackager } from "./packaging/JsonHttpMessagePackager";

export class HttpService {
  private _urlTransformer: HttpUrlTransformer = new SimpleHttpUrlTransformer();
  private _packager: HttpMessagePackager = new JsonHttpMessagePackager();
  private _headers: { [key: string]: string; } = {};

  withPackager(packager: HttpMessagePackager) {
    if (packager == null) {
      throw new Error("The parameter 'packager' must be specified.");
    }

    this._packager = packager;
  }

  withHeaders<T extends {}>(headers: T) {
    Object.assign(this._headers, headers);
  }

  withUrlTransformer(transformer: HttpUrlTransformer) {
    if (!transformer) {
      throw new Error("The parameter 'transformer' must be specified.");
    }

    this._urlTransformer = transformer;
  }

  protected mapUrl(url: string, queryParameters?: any): string {
    return this._urlTransformer.map(url, queryParameters);
  }

  private fetch<T>(url: string, method: "GET" | "POST" | "PUT" | "DELETE", body: any, packager: HttpMessagePackager = this._packager, headers?: any): Promise<T> {
    const request: RequestInit = {
      method,
      headers: headers ? Object.assign({}, this._headers, headers) : this._headers,
      body: body ? packager.pack(body) : null
    };

    const response = fetch(url, request);

    return packager.unpack(response);
  }

  sendGet<T>(url: string, queryParameters?: any, packager?: HttpMessagePackager): Promise<T> {
    return this.fetch<T>(
      this.mapUrl(url, queryParameters),
      "GET",
      null,
      packager);
  }

  sendPost<T>(url: string, data?: any, queryParameters?: any, packager?: HttpMessagePackager): Promise<T> {
    return this.fetch<T>(
      this.mapUrl(url, queryParameters),
      "POST",
      data,
      packager);
  }

  sendPut<T>(url: string, data?: any, queryParameters?: any, packager?: HttpMessagePackager): Promise<T> {
    return this.fetch<T>(
      this.mapUrl(url, queryParameters),
      "PUT",
      data,
      packager);
  }

  sendDelete<T>(url: string, data?: any, queryParameters?: any, packager?: HttpMessagePackager): Promise<T> {
    return this.fetch<T>(
      this.mapUrl(url, queryParameters),
      "DELETE",
      data,
      packager);
  }

}

