import { HttpUrlTransformer } from "./HttpUrlTransformer";

export class SimpleHttpUrlTransformer implements HttpUrlTransformer {

  map(url: string, query?: any): string {
    if (!query) {
      return url;
    }

    const pairs = Object.keys(query).map(key => {
      const value = query[key];

      return { key, value };
    });

    let parameters = pairs
      .reduce((current, next, index) => {
        if (index > 0) {
          current += "&";
        }

        if (Array.isArray(next.value)) {
          let values = next.value.join(`&${next.key}[]=`);
          current += `${next.key}[]=${values}`;
        } else {
          current += next.key + "=" + next.value;
        }

        return current;
      }, "");

    return url + parameters;
  }

}
