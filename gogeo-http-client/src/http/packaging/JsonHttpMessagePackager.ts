import { HttpMessagePackager } from "./HttpMessagePackager";

export class JsonHttpMessagePackager implements HttpMessagePackager {
  pack<T>(value: T): BodyInit {
    return JSON.stringify(value);
  }

  unpack<T>(promise: Promise<IResponse>): Promise<T> {
    return promise.then(response => <Promise<T>>response.json());
  }
}
