
/**
 * Responsible for packing messages to a http request and 
 * unpacking messagens from a http response.
 * 
 * You can use this as a custom serializer of messages, if you need.
 */
export interface HttpMessagePackager {
  
  /**
   * Packs the values as a BodyInit to be send by http, via fetch. 
   */
  pack<T>(value: T): BodyInit;

  /**
   * Unpacks the response in the promise, returning only the extracted
   * value of the message.
   */
  unpack<T>(promise: Promise<IResponse>): Promise<T>;
}
