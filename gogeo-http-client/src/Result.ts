export const nothing = {};
export type Nothing = typeof nothing;

export class Success<T> {
  type: "Success" = "Success";

  constructor(public readonly data: T) {

  }
}

export class Failure {
  type: "Failure" = "Failure";

  constructor(public readonly error: Error) {

  }
}

export type Result<T> = Success<T> | Failure;

export function success<T>(data: T): Success<T> {
  return new Success(data);
}

export function failure(error: Error): Failure {
  return new Failure(error);
}
