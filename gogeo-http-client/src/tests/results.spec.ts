import { Result, success, failure } from "../index";

describe("Um resultado", () => {
  it("pode ser uma falha", () => {
    const result: Result<any> = failure(Error("Falhou"));

    expect(result.type).toBe("Failure");
  });

  it("pode ser um sucesso", () => {
    const message = "Yay!";
    const result: Result<any> = success(message);

    expect(result.type).toBe("Success");
    expect(result.data).toBe(message);
  });
});
