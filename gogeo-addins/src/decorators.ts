import { autorun, action } from "mobx";
import { AddinManager } from "./AddinManager";

export interface ExtensionHostConfig {
  path: string;
  propertyName: string;
  manager?: AddinManager;
}

export function extensionHost(config: ExtensionHostConfig) {
  return (target: any) => {
    return new Proxy(target, {
      construct(target, args) {
        const manager = config.manager || AddinManager.instance;
        const instance = Reflect.construct(target, args);
        const extensionPath = manager.watchNodesByPath<any>(config.path);

        const disposer = autorun(
          () => {
            const activeNodes = extensionPath.activeNodes;

            const updateAction = action(`updating property ${config.propertyName} from extension path '${config.path}'`, () => {
              instance[config.propertyName] = activeNodes.reduce((prev, next) => prev.concat(next.data), []);
            });

            updateAction();
          }
        );

        const oldDispose = instance.dispose;

        instance.dispose = function () {
          disposer();

          if (oldDispose) {
            oldDispose.call(instance);
          }
        };

        return instance;
      }
    });
  };
}
