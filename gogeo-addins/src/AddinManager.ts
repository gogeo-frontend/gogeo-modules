import { observable, computed, action } from "mobx";
import { Addin } from "./Addin";
import { ExtensionNode } from "./ExtensionNode";
import { ExtensionNodeGroup } from "./ExtensionNodeGroup";

export class AddinManager {

  private static _instance: AddinManager;

  @observable
  private _addins: Addin[] = [];

  public static get instance(): AddinManager {
    return this._instance || (this._instance = new AddinManager());
  }

  @computed
  public get addins(): Addin[] {
    return this._addins;
  }

  @action
  public clear(): void {
    this._addins = [];
  }

  @action
  public add(addin: Addin): void {
    if (!addin) {
      throw Error("Parameter addin must be defined");
    }

    const exists = this.contains(addin);

    if (exists) {
      throw Error("Trying to add a duplicated Addin: " + addin.name);
    }

    this._addins.push(addin);
  }

  public contains(addin: Addin | string): boolean {
    if (!addin) {
      return false;
    }

    const addinName =
      addin instanceof Addin
        ? addin.name
        : addin;

    return !!this._addins.find(x => x.name == addinName);
  }

  public findNodesByPath<T>(path: string): ExtensionNode<T>[] {
    return this._addins
      .map(x => x.findNodesByPath<T>(path))
      .reduce((prev, next) => prev.concat(next), []);
  }

  public findNodeByPath<T>(path: string): ExtensionNode<T> {
    for (const addin of this._addins) {
      const found = addin.findNodeByPath<T>(path);

      if (found != null) {
        return found;
      }
    }

    return null;
  }

  public watchNodesByPath<T>(path: string): ExtensionNodeGroup<T> {
    return new ExtensionNodeGroup(() => this.findNodesByPath<T>(path));
  }
}
