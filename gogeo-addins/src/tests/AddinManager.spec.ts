import { AddinManager, Addin } from "../index";
import { ExtensionNode } from "../ExtensionNode";
import { autorun } from "mobx";

describe("AddinManager", () => {
  beforeEach(() => {
    AddinManager.instance.clear();
  });

  it("should be a singleton instance", () => {
    const instance1 = AddinManager.instance;
    const instance2 = AddinManager.instance;

    expect(instance1).toBe(instance2);
  });

  it("should not allow to register a null addin", () => {
    const manager = AddinManager.instance;

    try {
      manager.add(null);
    } catch (e) {
      expect(e.message).toBe("Parameter addin must be defined");
    }
  });

  it("should allow to clear the registered addins", () => {
    const addin = new class BeforeAddin extends Addin {
      constructor() {
        super("BeforeAddin");
      }
    };

    const manager = AddinManager.instance;

    manager.add(addin);

    expect(manager.addins.length).toBe(1);

    manager.clear();

    expect(manager.addins.length).toBe(0);
  });

  it("should allow to register addins", () => {
    const addin = new class BeforeAddin extends Addin {
      constructor() {
        super("BeforeAddin");
      }
    };

    const manager = AddinManager.instance;

    manager.add(addin);

    expect(manager.contains(addin)).toBeTruthy();
  });

  it("should not allow two addins with the same name", () => {
    const addin1 = new class BeforeAddin extends Addin {
      constructor() {
        super("Addin1");
      }
    };

    const addin2 = new class BeforeAddin extends Addin {
      constructor() {
        super("Addin1");
      }
    };

    const manager = AddinManager.instance;

    manager.add(addin1);

    try {
      manager.add(addin2);
      expect(false).toBeTruthy();
    } catch (e) {
      expect(e.message).toBe("Trying to add a duplicated Addin: Addin1");
    }
  });

  it("should return false when passing null to contains", () => {
    const manager = AddinManager.instance;
    const result = manager.contains(null);

    expect(result).toBeFalsy();
  });

  it("should find addin's node by path", () => {
    const manager = AddinManager.instance;
    const hello1Extension = new ExtensionNode("/Messages/Hello", "hello");
    const hello2Extension = new ExtensionNode("/Messages/Other", "hello");

    class SomeAddin extends Addin {
      constructor() {
        super("MyAddin");
        this.add(hello1Extension);
        this.add(hello2Extension);
      }
    }

    const addin = new SomeAddin();

    manager.add(addin);

    const foundNode = manager.findNodeByPath("/Messages/Hello");
    const foundNodes = manager.findNodesByPath("/Messages/Hello");

    expect(foundNode).toBe(hello1Extension);
    expect(foundNodes.length).toBe(1);
    expect(manager.findNodeByPath("/None")).toBeNull();
  });

  it("should allow more than one addin per path", () => {
    const manager = AddinManager.instance;
    const hello1Extension = new ExtensionNode("/Messages/Hello", "hello");
    const hello2Extension = new ExtensionNode("/Messages/Hello", "hello2");

    class SomeAddin extends Addin {
      constructor() {
        super("MyAddin");
        this.add(hello1Extension);
      }
    }

    class AnotherAddin extends Addin {
      constructor() {
        super("MyAddin2");
        this.add(hello2Extension);
      }
    }

    manager.add(new SomeAddin());
    manager.add(new AnotherAddin());

    const foundNodes = manager.findNodesByPath("/Messages/Hello");

    expect(foundNodes.length).toBe(2);
    expect(foundNodes).toContain(hello1Extension);
  });

  it("should only return active nodes", () => {
    const manager = AddinManager.instance;
    const hello1Extension = new ExtensionNode("/Messages/Hello", "hello");
    const hello2Extension = new ExtensionNode("/Messages/Hello", "hello2");

    class SomeAddin extends Addin {
      constructor() {
        super("MyAddin");
        this.add(hello1Extension);
        this.add(hello2Extension);
      }
    }

    manager.add(new SomeAddin());
    hello2Extension.active = false;

    const foundNodes = manager.findNodesByPath("/Messages/Hello");

    expect(foundNodes.length).toBe(1);
    expect(foundNodes).toContain(hello1Extension);
    expect(foundNodes).not.toContain(hello2Extension);
  });

  it("should be able to observe a group of extensions", () => {
    const manager = AddinManager.instance;
    const hello1Extension = new ExtensionNode("/Messages/Hello", "hello");
    const hello2Extension = new ExtensionNode("/Messages/Hello", "hello2");

    class SomeAddin extends Addin {
      constructor() {
        super("MyAddin");
        this.add(hello1Extension);
        this.add(hello2Extension);
      }
    }

    manager.add(new SomeAddin());
    
    const group = manager.watchNodesByPath<string>("/Messages/Hello");

    const done = new Promise<boolean>(resolve => {
      let firstCall = true;

      autorun(() => {
        const nodes = group.activeNodes;
        
        if (firstCall) {
          firstCall = false;
          return;
        }

        expect(nodes.length).toBe(1);
        expect(nodes).toContain(hello1Extension);
        resolve(true);
      });
    });

    hello2Extension.active = false;

    return done;
  });
});
