import { ExtensionNode } from "../index";

describe("The ExtensionNode", () => {
  it("should return the path from toString()", () => {
    const node = new ExtensionNode("/path", "b");

    expect(node.toString()).toBe(node.path);
  });

  it("should allow me to create an object like in Mono.Addin", () => {
    const nodePath = "/Name";
    const nodeValue = "Daniel";
    const nodeIsActive = true;
    const node = new ExtensionNode(nodePath, nodeValue, nodeIsActive);

    expect(node.path).toBe(nodePath);
    expect(node.data).toBe(nodeValue);
    expect(node.active).toBe(true);
  });
});
