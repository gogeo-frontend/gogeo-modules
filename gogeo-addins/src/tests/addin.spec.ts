import { Addin, ExtensionNode } from "../index";

describe("The Addin", () => {
  it("should define a name and version", () => {
    const addin = new Addin("MyAddin");

    expect(addin.name).toBe("MyAddin");
    expect(addin.version).toBe(1);
  });

  it("should allow me to define extension nodes", () => {
    const addin = new Addin("MyAddin");
    const extensionNode = new ExtensionNode("/Name", "Daniel");

    addin.add(extensionNode);

    expect(addin.contains(extensionNode));
    expect(addin.extensionNodes.indexOf(extensionNode) >= 0).toBeTruthy();
  });

  it("should allow me to retrieve extension nodes registered for a path", () => {
    const addin = new Addin("MyAddin");
    const extensionNode = new ExtensionNode("/Name", "Daniel");

    addin.add(extensionNode);

    const nodes = addin.findNodesByPath("/Name");

    expect(nodes.length).toBe(1);
    expect(nodes.indexOf(extensionNode) >= 0).toBe(true);
  });
});
