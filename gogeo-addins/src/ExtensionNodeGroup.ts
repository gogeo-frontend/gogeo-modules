import { ExtensionNode } from "./ExtensionNode";
import { computed } from "mobx";

export class ExtensionNodeGroup<T> {
  constructor(private _producer: () => ExtensionNode<T>[]) {

  }

  @computed
  get activeNodes(): ExtensionNode<T>[] {
    return this._producer();
  }
}
