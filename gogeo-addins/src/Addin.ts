import { ExtensionNode } from "./ExtensionNode";
import { observable, computed } from "mobx";

export class Addin {

  @observable
  private readonly _extensionNodes = Array<ExtensionNode<any>>();

  constructor(public readonly name: string,
    public readonly version: number = 1) {

  }

  @computed
  public get extensionNodes() {
    return this._extensionNodes;
  }

  @computed
  public get activeNodes() {
    return this._extensionNodes.filter(x => x.active);
  }

  public findNodesByPath<T>(path: string): ExtensionNode<T>[] {
    return <ExtensionNode<T>[]>this.activeNodes.filter(x => x.path == path);
  }

  public findNodeByPath<T>(path: string): ExtensionNode<any> {
    return <ExtensionNode<T>>this.activeNodes.find(x => x.path == path);
  }

  public add(node: ExtensionNode<any>) {
    return this._extensionNodes.push(node);
  }

  public contains(node: ExtensionNode<any>) {
    return this._extensionNodes.indexOf(node) >= 0;
  }

}
