import { observable } from "mobx";

/**
 * Created by danfma on 08/09/16.
 */

export class ExtensionNode<T> {

  @observable
  public data: T;

  @observable
  public active: boolean;

  constructor(public readonly path: string, data: T, active: boolean = true) {
    this.data = data;
    this.active = active;
  }

  public toString(): string {
    return `${this.path}`;
  }
  
}
