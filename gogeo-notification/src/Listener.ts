import * as Rx from "rxjs/Rx";

export interface Listener {
  addSubscription(subscription: Rx.Subscription): void;
  removeSubscription(subscription: Rx.Subscription): void;
}
