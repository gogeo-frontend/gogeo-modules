import * as Rx from "rxjs/Rx";

export interface Notification {
  type: string;
}

export class NotificationPackage {
  constructor(
    public readonly sender: any,
    public readonly notification: Notification) { }
}

export class Notifier {
  private static _instance: Notifier;
  private _notifications$ = new Rx.Subject<NotificationPackage>();
  private _exposedNotifications$ = this._notifications$.share();

  private constructor() {

  }

  static get instance(): Notifier {
    return this._instance || (this._instance = new Notifier());
  }

  get notifications$(): Rx.Observable<NotificationPackage> {
    return this._exposedNotifications$;
  }

  send(sender: any, notification: Notification) {
    this._notifications$.next(new NotificationPackage(sender, notification));
  }

}
