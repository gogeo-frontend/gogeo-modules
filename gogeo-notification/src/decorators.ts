import { Notifier, NotificationPackage } from "./Notifier";
import { Listener } from "./Listener";
import * as Rx from "rxjs/Rx";

export function isListener(something: any): something is Listener {
  if (!something) {
    return false;
  }

  const hasAddSubscription = typeof something["addSubscription"] === "function";
  const hasRemoveSubscription = typeof something["removeSubscription"] === "function";

  return hasAddSubscription && hasRemoveSubscription;
}

class SubscriptionTable {
  private _subscriptions: { [key: string]: Rx.Subscription; } = {};

  dispose() {
    this.clear();
  }

  clear() {
    const keys = Object.getOwnPropertyNames(this._subscriptions);

    for (const key of keys) {
      const subscription = this._subscriptions[key];

      if (subscription) {
        subscription.unsubscribe();
        this._subscriptions[key] = undefined;
      }
    }
  }

  has(type: string): boolean {
    return this._subscriptions[type] ? true : false;
  }

  add(type: string, subscription: Rx.Subscription): void {
    if (subscription) {
      if (!this.has(type)) {
        this._subscriptions[type] = subscription;
      }
    }
  }
}

const HANDLED_NOTIFICATION_KEYS = "$handledNotifications$";

interface NotificationHandler {
  propertyKey: string;
  transformer?: (source: Rx.Observable<NotificationPackage>) => Rx.Observable<NotificationPackage>;
}

export function receive(notificationType: string, transformer?: (source: Rx.Observable<NotificationPackage>) => Rx.Observable<NotificationPackage>) {
  return (target: any, propertyKey: string, _: PropertyDescriptor) => {
    let handledNotifications = target[HANDLED_NOTIFICATION_KEYS];

    if (!handledNotifications) {
      Object.defineProperty(target, HANDLED_NOTIFICATION_KEYS, {
        configurable: false,
        enumerable: false,
        value: {}
      });

      handledNotifications = target[HANDLED_NOTIFICATION_KEYS];
    }

    handledNotifications[notificationType] = <NotificationHandler>{
      propertyKey,
      transformer
    };
  };
}

function getOrCreateSubscriptionTable(instance: any): SubscriptionTable {
  const key = "$notificationSubscriptions$";
  let subscriptionTable: SubscriptionTable = instance[key];

  if (!subscriptionTable) {
    // Criando a propriedade que irá conter a tabela de inscrições de notificações
    Object.defineProperty(instance, key, {
      enumerable: false,
      configurable: false,
      value: new SubscriptionTable()
    });

    subscriptionTable = instance[key];
  }

  const originalDisposer = instance.dispose;

  // Criando um método para remover as inscrições desta instância.
  instance.dispose = function () {
    subscriptionTable.dispose();

    if (typeof originalDisposer === "function") {
      originalDisposer();
    }
  };

  return subscriptionTable;
}

function logReceipt(observer: any, method: string, notificationPackage: NotificationPackage) {
  if (!console) {
    return;
  }

  if (!console.groupCollapsed) {
    console.log("!! NOTIFICATION !!");
    console.log("handled by method: ", method);
    console.log("observer: ", observer);
    console.log("sender: ", notificationPackage.sender);
    console.log("notification: ", notificationPackage.notification);
    console.log();
    return;
  }

  const notificationType = notificationPackage.notification && notificationPackage.notification.type || "(Não identificada)";

  console.groupCollapsed.call(console,
    "%cNOTIFICATION %c%s%s",
    "background-color: orange; font-weight: bold; color: #333; padding: 4px; 6px",
    "background-color: #D7D6D7; color: #333; padding: 4px; 6px",
    new Date().toLocaleTimeString(),
    notificationType);
    
  console.log("%cHandled by method: ", "font-weight: bold", method);
  console.log("%cObserver: ", "font-weight: bold", observer);
  console.log("%cSender: ", "font-weight: bold", notificationPackage.sender);
  console.log("%cNotification: ", "font-weight: bold", notificationPackage.notification);
  console.groupEnd();
}

function subscribeToNotification(
  handledNotifications: { [key: string]: NotificationHandler },
  subscriptionTable: SubscriptionTable,
  notificationType: string,
  instance: any) {

  const handler: NotificationHandler = handledNotifications[notificationType];
  const watchedNotificationType = notificationType;
  const methodName = handler.propertyKey;
  const transformer = handler.transformer;
  const notifier = Notifier.instance;

  if (!subscriptionTable.has(watchedNotificationType)) {
    let source$ = notifier.notifications$
      .filter(x => {
        return x.notification.type == watchedNotificationType;
      });

    if (transformer) {
      source$ = transformer(source$) || source$;
    }

    const subscription = source$.subscribe(x => {
      logReceipt(instance, methodName, x);
      instance[methodName](x.notification, x.sender);
    });

    subscriptionTable.add(watchedNotificationType, subscription);
  }
}

function registerToNotifications(instance: any) {
  const subscriptionTable = getOrCreateSubscriptionTable(instance);
  const handledNotifications = instance[HANDLED_NOTIFICATION_KEYS];

  if (handledNotifications) {
    for (const notificationType in handledNotifications) {
      subscribeToNotification(handledNotifications, subscriptionTable, notificationType, instance)
    }
  }
}

export function listener(target: any) {
  const original = target;

  function construct(constructor: any, args: any[]) {
    const x: any = function () {
      return constructor.apply(this, args);
    };
    x.prototype = constructor.prototype;
    return new x();
  }

  const factory: any = function (...args: any[]) {
    const instance = construct(original, args);
    registerToNotifications(instance);

    return instance;
  }

  return factory;
}

export function makeListener(instance: any) {
  registerToNotifications(instance);
  return instance;
}
