import { Notifier } from "../Notifier";
import * as Rx from "rxjs/Rx";

describe("O Notifier", () => {
  it("deve publicar notificações para qualquer listener registrado", () => {
    const notifier = Notifier.instance;
    const done = new Rx.Subject<boolean>();

    notifier.notifications$
      .delay(100)
      .subscribe({
        next: (x) => {
          const notification = x.notification;
          
          expect(notification.type).toBe("abc");
          done.next(true);
          done.complete();
        },

        error: (error) => {
          done.error(error);
        }
      });

    notifier.send(this, { type: "abc" });

    return done.toPromise();
  });
});
