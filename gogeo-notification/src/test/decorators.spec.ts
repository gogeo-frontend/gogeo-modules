import { receive, listener, Notifier, Notification } from "../index";
import * as Rx from "rxjs/Rx";
import { makeListener } from "./../decorators";

class MyNotification implements Notification {
  type: "I'm the bone of my sword" = "I'm the bone of my sword";
}

@listener
class Listener {
  done = new Rx.Subject<boolean>();

  dispose() {

  }

  @receive("I'm the bone of my sword")
  protected onAbc(notification: Notification) {
    try {
      expect(notification.type).toBe("I'm the bone of my sword");

      this.done.next(true);
      this.done.complete();

    } catch (e) {
      this.done.error(e);
    }
  }
}

describe("Decorators de notificação", () => {
  it("devem inscrever métodos de classes, para escutar o notificador", () => {
    const notifier = Notifier.instance;
    const listener = new Listener();
    const done = listener.done.toPromise();

    notifier.send(this, new MyNotification());

    return done;
  });

  it("devem aceitar uma instância estendê-la", () => {
    const called$ = new Rx.BehaviorSubject(0);

    class X {
      count = 0;

      dispose() {

      }

      @receive("INLINE")
      protected onReceiveInline(notification: Notification) {
        expect(notification).not.toBeNull();
        expect(notification.type).toBe("INLINE");
        called$.next(++this.count);
      }
    }

    const notifier = Notifier.instance;
    const instance = new X();
    const notification: Notification = { type: "INLINE" };

    expect(instance.count).toBe(0);

    makeListener(instance);
    notifier.send(this, notification);

    expect(instance.count).toBe(1);
    
    notifier.send(this, notification);

    expect(instance.count).toBe(2);

    instance.dispose();
    notifier.send(this, notification);

    expect(instance.count).toBe(2);
  });
});
